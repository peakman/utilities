//
//  error_handling.swift
//  
//
//  Created by Steve Clarke on 02/07/2017.
//
//

import Foundation

public enum Result<T> {
    case success(T)
    case failure(SimpleError)
}


public enum SimpleErrorDomain : String {
    case general
    case http
    case login
}

public enum SimpleErrorCode : Int {
    // General Errors
    case missing = 0
    // HTTP Errors
    // Login Errors
}

public enum SimpleError : Error {
    case swift(msg: String, domain: SimpleErrorDomain?, code: SimpleErrorCode?, info: StrAny?  )
    case nserror(error: NSError, msg: String?)
        
    public init( msg: String, domain: SimpleErrorDomain? = nil, code: SimpleErrorCode? = nil, info: StrAny? = nil) {
        self = SimpleError.swift(msg: msg, domain: domain, code: code, info:  info)
    }
    
    public init(error: NSError, msg: String? = nil) {
        self = SimpleError.nserror(error: error, msg: msg)
    }
    
    public var nsError : NSError? {get{
        switch self {
        case .swift:
            return nil
        case let .nserror(err, _):
            return err
        }
    }}
    
    public var domain : Any? {get{
        switch self {
        case .swift(_, let dom, _, _):
            return dom
        case let .nserror(err, _):
            return err.domain
        }
        }}
    
    public var domainString : String? {get{
        switch self {
        case .swift(_, let dm, _, _):
            if dm != nil {
                return dm!.rawValue
            } else {
                return nil
            }
        case let .nserror(err, _):
            return err.domain
        }
    }}
    
    public var code : Any? {get{
        switch self {
        case .swift(_, _, let code, _):
            return code
        case let .nserror(err, _):
            return err.code
        }
        }}
    
    public var codeInt : Int? {get{
        switch self {
        case .swift(_, _, let code, _):
            if code != nil {
                return code?.rawValue
            } else {
                return nil
            }
        case let .nserror(err, _):
            return err.code
        }
        }}
    
    public var info: StrAny? {get{
        switch self {
        case .swift(_,_,_,let info):
            return info
        case let .nserror(err, _):
            if err.userInfo.count == 0 {
                return nil
            } else {
                return err.userInfo
            }
        }
    }}
    
    public var msg : String { get{
        var msg : String
        switch self {
        case let .swift(msgs,_,_,_):
            msg = msgs
        case let .nserror(err, msge):
            if msge == nil {
                msg = err.localizedDescription
            } else {
                msg = msge!
            }
        }
        return msg
    }}
}

extension SimpleError : LocalizedError {
    public var errorDescription: String? {
        return self.msg
    }

}


