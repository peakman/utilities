//
//  free_standing_functions.swift
//  
//
//  Created by Steve Clarke on 02/07/2017.
//
//

import Foundation

public typealias StrAny = [String : Any]


#if !DEBUG
    public func debugPrint(items: Any..., separator: String = " ", terminator: String = "\n") { }
    public func print(_ items: Any..., separator: String = " ", terminator: String = "\n") { }
#endif


public func optFromAny<T>(_ v: Any?) -> T? {
    return  (v as? NSNull  == NSNull()) ? nil : v as? T
}

