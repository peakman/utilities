//
//  json_decoder.swift
//  Bamford2WebApi
//
//  Created by Steve Clarke on 10/04/2018.
//  Copyright © 2018 Steve Clarke. All rights reserved.
//

import Foundation 

public class JSONDecoderSC {
    let decoder = JSONDecoder()
    func decode<T>(_ type: T.Type, from data: Data) -> T where T : Decodable {
        let decodedResult : T
        do {
            decodedResult = try decoder.decode(type, from: data)
        } catch {
            print("error decoding json: ", error)
            fatalError("terminating webapi processor")
        }
        return decodedResult
    }
}
