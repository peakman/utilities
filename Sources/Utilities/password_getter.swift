//
//  password_getter.swift
//  UsefulCrossPlatform
//
//  Created by Steve Clarke on 09/06/2018.
//

import Foundation
import Security

public func findServicePassword(_ service: String, accountName: String) throws -> String {
    let query : [CFString: Any] = [
        kSecClass: kSecClassGenericPassword,
        kSecAttrService: service ,
        kSecAttrAccount: accountName,
        kSecReturnAttributes: false,
        kSecReturnRef: false,
        kSecReturnData: true
    ]
    var itemq: CFTypeRef?
    let status = SecItemCopyMatching(query as CFDictionary, &itemq)
    let errorMsg : CFString? = SecCopyErrorMessageString(status, nil)
    let stringMsg = errorMsg == nil ? "unknown error " : errorMsg.debugDescription
    guard status != errSecItemNotFound else { throw SimpleError(msg: "\(stringMsg) for \(accountName) at \(service)") }
    guard status == errSecSuccess else { throw SimpleError(msg: "Unhandled sec search error: \(stringMsg)") }
    guard itemq != nil else {throw SimpleError(msg: "Password item is nil")}
    guard let pw = String(data: itemq! as! Data, encoding: .utf8) else {throw SimpleError(msg: "Failed to convert password item to String")}
    return pw
}
public func addTestPassword(service: String, account: String, pw: String) -> Bool {
    let query : [CFString: Any] = [
        kSecClass: kSecClassGenericPassword,
        kSecAttrService: service ,
        kSecAttrAccount: account,
        kSecReturnAttributes: false,
        kSecReturnRef: false,
        kSecReturnData: true
    ]
    let status = SecItemDelete(query as CFDictionary)
    guard status == errSecItemNotFound || status == errSecSuccess   else {
        fatalError( "Password to delete is not found or was found but not deleted. Status is \(status)") }
    let dict : [CFString: Any] = [
        kSecClass: kSecClassGenericPassword,
        kSecValueData: pw.data(using: .utf8)!,
        kSecAttrAccount: account,
        kSecAttrService: service
    ]
    var result: CFTypeRef?
    let res = SecItemAdd(dict as CFDictionary, &result)
    guard res == errSecSuccess else { fatalError("Failed to add keychain item for \(account) with password \(pw) at service \(service)") }
    print("Added keychain item for \(account) with password \(pw) at service \(service)")
    return true
}


