//
//  plist_reader.swift
//  eraseBackupVolume
//
//  Created by Steve Clarke on 05/04/2018.
//  Copyright © 2018 Steve Clarke. All rights reserved.
//

import Foundation

public class PlistReader {
    let plistDir : URL
    public init(_ plDir: URL? = nil) {
        if let pld = plDir {
            self.plistDir = pld
        } else {
            self.plistDir = PlistReader.defaultPlistDir()
        }
    }
    public func readPlist<T>(_ type: T.Type, name: String) -> T where T : Decodable {
        let plistURL = plistDir.appendingPathComponent(name, isDirectory: false)
        let plistData: Data
        do {
            plistData = try Data(contentsOf: plistURL)
        } catch {
            fatalError(error.localizedDescription)
        }
//        guard plistData != nil else {
//            fatalError("Unable to read plist \(name)")
//        }
        print("plist data : ", plistData)
        let propertyList : T
        do {
            let decoder = PropertyListDecoder()
            propertyList = try decoder.decode(type, from: plistData)
        } catch {
            print("error decoding plist \(name): ", error)
            fatalError("terminating readPlist")
        }
        return propertyList
    }
    
    public class func defaultPlistDir() -> URL {
        let scsh_common_dir : URL
        do {
            scsh_common_dir =  try FileManager.default.url(for: .libraryDirectory, in: .userDomainMask, appropriateFor: nil, create: false)
                .appending(components:  "Mobile Documents", "com~apple~CloudDocs", "Documents","pcadmin", "plists", directoryHint: .isDirectory)
        } catch {
            fatalError("Error getting common dir: \(error.localizedDescription)")
        }
        return scsh_common_dir
    }
}
