//
//  regex.swift
//  UsefulCrossPlatform
//
//  Created by Steve Clarke on 31/01/2018.
//

import Foundation

public struct Regex {
    let pattern: String
    let options: NSRegularExpression.Options
    
    private var matcher: NSRegularExpression {
        do {
            return try NSRegularExpression(pattern: self.pattern, options: self.options)
        } catch   {
            fatalError("\(error)")
        }
    }
    
    public init(pattern: String, options: NSRegularExpression.Options = []) {
        self.pattern = pattern
        self.options = options
    }
    
    public func match(_ string: String, options: NSRegularExpression.MatchingOptions = []) -> Bool {
        return self.matcher.numberOfMatches(in: string,
                                            options: options,
                                            range: NSMakeRange(0, string.utf16.count)) != 0
    }
}

extension Regex: ExpressibleByStringLiteral {
    public typealias ExtendedGraphemeClusterLiteralType = StringLiteralType
    
    
    public init(extendedGraphemeClusterLiteral value: ExtendedGraphemeClusterLiteralType) {
        self.pattern = value
        self.options = []
    }
    
    public init(stringLiteral value: StringLiteralType) {
        self.pattern = value
        self.options = []
    }
}



