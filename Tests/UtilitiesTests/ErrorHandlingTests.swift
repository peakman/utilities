//
//  ErrorHandlingTests.swift
//  UtilitiesTests
//
//  Created by Steve Clarke on 05/06/2019.
//

import XCTest
import Utilities

class ErrorHandlingTests: XCTestCase {
    var error = SimpleError(msg: "Hello")

    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }
    

    func testMsg() {
        XCTAssertEqual(error.msg , "Hello")
    }

    func testLocalizedDescription() {
        XCTAssertEqual(error.localizedDescription , "Hello")
    }
   
}
