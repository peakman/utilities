//
//  PasswordGetterTests.swift
//  
//
//  Created by Steve Clarke on 25/05/2022.
//

import XCTest
import Utilities

class PasswordGetterTests: XCTestCase {
    var error = SimpleError(msg: "Hello")

    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }
    
    func testAddServicePassword() throws {
        _ = addTestPassword(service: "careline.dev", account: "test", pw: "xyz")
    }

    func testGetServicePassword() throws {
        let pw = try findServicePassword("careline.dev", accountName: "test")
        XCTAssertEqual(pw, "xyz")
    }

}
