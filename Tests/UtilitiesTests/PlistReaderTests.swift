//
//  PlistReaderTests.swift
//  UtilitiesTests
//
//  Created by Steve Clarke on 16/02/2023.
//

import XCTest
import Utilities

final class PlistReaderTests: XCTestCase {

    func testDefaultDir() throws {
        XCTAssertNoThrow(PlistReader.defaultPlistDir())
        let dir : URL = PlistReader.defaultPlistDir()
        XCTAssertNotNil(dir)
        print(dir)
    }

}
